

document.getElementById("addApple").addEventListener("mousedown", AddA);

document.getElementById("addMango").addEventListener("mousedown", AddM);

document.getElementById("addNewItem").addEventListener("mousedown", NewItem);

document.getElementById("showB").addEventListener("mousedown", ShowB);

var cart = [];

var Item = function(name, price, count) {
  this.name = name;
  this.price = price;
  this.count = count;
};



//add item to cart
function addItem(name, price, count){
  for (var key in cart) {
    if (cart[key].name === name) {
      cart[key].count += count;
      cart[key].price += price;
      return;
    }
  }
  var item = new Item(name, price, count);
  cart.push(item);
}

// count item amount
function cartcount() {
  var x = 0;
  for (var key in cart) {
    x += cart[key].count;
  }
  document.getElementById("item").innerHTML = "Item(s):" + x;
  var cartcount = (sessionStorage.cartcount ? JSON.parse(sessionStorage.cartcount):[]);
  sessionStorage.setItem("cartcount",JSON.stringify(x));
  console.log(cartcount);
}

// calculate price
function cartprice() {
  var d = 0;
  for (var key in cart) {
      d += cart[key].price;
  }
  document.getElementById("dollar").innerHTML = "$HKD: " + d;
  var itemd = (sessionStorage.itemd ? JSON.parse(sessionStorage.itemd):[]);
  sessionStorage.setItem("itemd",JSON.stringify(d));

  console.log(itemd);
}

//remove the session storage in the broswer
window.onbeforeunload = function() {
  Cart = sessionStorage.removeItem("cartcount");
  Cart = sessionStorage.removeItem("itemd");
};

// display items from alert box
function ShowB() {
  var iname = document.getElementById("name").innerText;
  var price = document.getElementById("dollar").innerText;
  var item = document.getElementById("item").innerText;

  alert(iname+"\n\n" +"price "+price+"\n\n"+item);
}

//add apple
function AddA() {
  addItem("Apple", 3, 1);
  cartcount();
  cartprice();
  addapple();
  addappleDes();
  alert("it is an Apple");
}

//add Mango
function AddM() {
  addItem("Mango", 5.5, 1);
  cartcount();
  cartprice();
  addmango();
  addmangoDes();
  alert("it is an Mango");
}

//add itemY
function AddY() {
  var Pname = document.getElementById("pname").value;
  var Des = document.getElementById("des").value;
  var nprice = document.getElementById("nprice");
  var Nprice = parseFloat(nprice.value);
  if (nprice.value === ""){
        alert();
  } else {
  addItem(Pname, Nprice, 1);
  cartcount();
  cartprice();
  addy();
  addyDes();
  }
}





// add new item
function NewItem() {
  var Pname = document.getElementById("pname").value;
  var Des = document.getElementById("des").value;
  var nprice = document.getElementById("nprice");
  var Nprice = parseFloat(nprice.value);

      console.log(nprice);
      console.log(Nprice);
  if (Pname ==""){
    alert("fill in fruit name");
  } else if (nprice.value == ""){

      alert("fill in price");
  } else if (Des ==""){
      alert("fill in descrption");
  } else {
    var btn = document.createElement("BUTTON");
    var t = document.createTextNode("Add "+ Pname  +" $ "+ Nprice);
    btn.appendChild(t);
    document.body.appendChild(btn);
    btn.id='addY';
    document.getElementById("addY").addEventListener("mousedown", AddY);
    alert("Add item");
  }
}


// add apple to the list
function addapple() {
  var a ="";
    for (var key in cart) {
      a = "Apple";
    }
  document.getElementById("name").innerHTML += "<p>" + a;
}

// add apple descrption to the list
function addappleDes() {
  var aad ="";
    for (var key in cart) {
      aad = "it is an apple";
    }
document.getElementById("description").innerHTML += "<p>" + aad;
}



// add mango to the list
function addmango() {
  var m ="";
    for (var key in cart) {
      m = "Mango";
    }
document.getElementById("name").innerHTML += "<p>" + m;
}

// add mango descrption to the list
function addmangoDes() {
  var amd ="";
    for (var key in cart) {
      amd = "it is a mango";
    }
document.getElementById("description").innerHTML += "<p>" + amd;
}




// add item y to the list
function addy() {
  var Pname = document.getElementById("pname").value;
  var y ="";
    for (var key in cart) {
      y = Pname;
    }
    document.getElementById("name").innerHTML += "<p>" + y;
}

// add y descrption to the list
function addyDes() {
  var Des = document.getElementById("des").value;
  var ayd ="";
    for (var key in cart) {
      ayd = Des;
    }
  document.getElementById("description").innerHTML += "<p>" + ayd;
}
